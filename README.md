# Validon Capital Fund #



### What is Validon Capital Fund? ###

**Validon Capital Fund** is a hybrid mutual fund that handles fiat assets and cryptocurrencies 
within **FusionFabric.cloud** and a **DLT** (**D**istributed **L**edger **T**echnology) platform.